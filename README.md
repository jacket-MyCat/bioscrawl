Bioscrawl
=========

### About ###

Bioscrawl is a molecular drawing program that aids users in learning biochemical structures. Users are given feedback on the correctness of their drawings.

The frontend is written in JavaScript using the React and D3 libraries. React components were bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

The backend is written in Python using Networkx isomorphism algorithms to evaluate user drawings and Flask to communicate with the frontend. 3D molecular geometry is not considered when determining drawing accuracy. 2D structural information was retrieved from [PubChem](https://pubchem.ncbi.nlm.nih.gov).

This code is released under the [GNU General Public License, version 3](https://opensource.org/licenses/GPL-3.0)

### Try It ###
[www.bioscrawl.com](https://www.bioscrawl.com)

### Python Dependencies ###

Ubuntu 20.04.1

Run the following command to install necessary system packages:

    sudo apt-get install -y python3-pip git

Install virtualenv using this command (note that you might need to add the installation location to your PATH environment variable afterwards):

    pip3 install virtualenv

Create a Python virtual environment like this:

    cd bioscrawl
    virtualenv venv

Start the virtual environment and install the needed Python libraries:

    source ./venv/bin/activate
    pip3 install -r requirements.txt

The virtual environment can be deactivated by typing ``deactivate``. Whenever
you need to reactivate it, just type the source command like you did earlier:

    source ./venv/bin/activate

### JavaScript Dependencies ###

Ubuntu 20.04.1

Run the following command to install a necessary system package:

    sudo apt-get install -y curl

Install and activate Node Version Manager (nvm) using these commands:

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
    . ~/.nvm/nvm.sh

Install Node using one of the following commands (your choice). Node version 12.17.0 was used during the production of Bioscrawl but later versions may also work:

    nvm install node
    nvm install v12.17.0

Install dependencies specific to Bioscrawl:

    npm install

### How to Use ###

Once all dependencies have been installed, enter the following command to start the Bioscrawl frontend:

    npm start

To start the backend, open another terminal window, navigate to the bioscrawl directory, activate the Python virtual environment, and initiate Flask:

    source ./venv/bin/activate
    flask run

Go to localhost:3000 in your browser. Reload.
