import * as d3 from "d3";
import multiBondMath from "./helpers";

class D3Component {
  constructor() {
    this.radius = d3.scaleSqrt().range([0, 6]);
  }

  createSimulation = (width, height) => {
    return d3
      .forceSimulation()
      .force(
        "link",
        d3
          .forceLink()
          .id((d) => d.id)
          .distance(
            (d) => this.radius(d.source.size) + this.radius(d.target.size) + 20
          )
      )
      .force("charge", d3.forceManyBody().strength(-200))
      .force(
        "collide",
        d3.forceCollide().radius((d) => this.radius(d))
      )
      .force("x", d3.forceX(width / 2))
      .force("y", d3.forceY(height / 2))
      .velocityDecay(0.3);
  };

  startSimWithDrag = (
    canvas,
    context,
    nodes,
    links,
    simulation,
    width,
    height
  ) => {
    const offset = 2;

    simulation.nodes(nodes).on("tick", () => {
      nodes.forEach((d) => {
        // keep node on canvas
        d.x = Math.max(
          0 + this.radius(d.size) + offset,
          Math.min(width - this.radius(d.size) - offset, d.x)
        );
        d.y = Math.max(
          0 + this.radius(d.size) + offset,
          Math.min(height - this.radius(d.size) - offset, d.y)
        );
      });

      this.draw(nodes, links, context, width, height);
    });

    simulation.force("link").links(links);

    d3.select(canvas)
      .on("mousedown", null)
      .on("mouseup", null)
      .call(
        d3
          .drag()
          .container(canvas)
          .subject(dragsubject)
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended)
      );

    function dragsubject() {
      return simulation.find(d3.event.x, d3.event.y);
    }

    function dragstarted() {
      if (!d3.event.active) simulation.alphaTarget(0.3).restart();
      d3.event.subject.fx = d3.event.subject.x;
      d3.event.subject.fy = d3.event.subject.y;
    }

    function dragged() {
      d3.event.subject.fx = Math.max(0, Math.min(width, d3.event.x));
      d3.event.subject.fy = Math.max(0, Math.min(height, d3.event.y));
    }

    function dragended() {
      if (!d3.event.active) simulation.alphaTarget(0);
      d3.event.subject.fx = null;
      d3.event.subject.fy = null;
    }
  };

  stopSimAndDrag = (canvas, simulation) => {
    simulation.stop();
    d3.select(canvas).on(".drag", null);
  };

  draw = (nodes, links, context, width, height) => {
    const radius = this.radius;
    const alpha = 0.5;
    const defaultWidth = 2;
    const selectWidth = 10;

    context.clearRect(0, 0, width, height);
    context.lineWidth = defaultWidth;
    links.forEach(drawLink);
    nodes.forEach(drawNode);

    function drawLink(d) {
      let source, target, r, cx;
      if (typeof d.source === "object") {
        // simulation is on and
        // D3 has inserted node
        // objects into source
        // and target keys
        source = d.source;
        target = d.target;
      } else {
        // simulation is off and
        // link array has not been
        // altered
        source = nodes.get(d.source);
        target = nodes.get(d.target);
      }

      r = Math.min(radius(source.size), radius(target.size));
      cx = radius(source.size) === r ? source.x : target.x;

      let bondArray = multiBondMath(
        source.x,
        source.y,
        target.x,
        target.y,
        r,
        cx,
        d.weight
      );

      for (const { x1, y1, x2, y2 } of bondArray) {
        context.beginPath();
        context.strokeStyle = "#aaa";
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();
        if (d.active) {
          context.lineWidth = selectWidth;
          context.strokeStyle = "rgba(26,188,156," + alpha.toString() + ")";
          context.stroke();
          // set line width back
          // to what is was previously
          context.lineWidth = defaultWidth;
        }
        context.closePath();
      }
    }

    function drawNode(d) {
      // Draw circle
      let r = radius(d.size);
      context.save();

      context.beginPath();
      context.moveTo(d.x + r, d.y);
      context.arc(d.x, d.y, r, 0, 2 * Math.PI);
      context.fillStyle = d.color;
      context.strokeStyle = "#727171";
      context.lineWidth = 4;
      context.stroke();
      context.fill();
      if (d.active) {
        context.lineWidth = selectWidth;
        context.strokeStyle = "rgba(26,188,156," + alpha.toString() + ")";
        context.stroke();
      }
      context.closePath();

      // Draw text symbol
      context.fillStyle = "#000";
      context.font = "bold 10px Arial";
      context.textAlign = "center";
      context.textBaseline = "middle";
      context.fillText(d.symbol, d.x, d.y);

      context.restore();
    }
  };
}

export default D3Component;
