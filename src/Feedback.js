import React from "react";

import * as c from "./constants.js";
import "./Feedback.css";

const Feedback = (props) => {
  if (props.testResult === c.NA) {
    return null;
  }

  const width = props.dimensions.width;
  const fdbkWidth = width / 2;
  const leftPx = props.dimensions.left + (width - fdbkWidth) / 2;
  const minHeight = "2vh";
  const topPx = props.dimensions.bottom - c.OFFSET;
  const classNames = ["feedback"];

  const giveUp = props.giveUp;
  const testResult = props.testResult;
  const type = props.type;
  let displayText;

  if (giveUp && type === c.ANSWER) {
    displayText = c.CORRECT;
    classNames.push("correct");
  } else if (!giveUp && testResult === c.SUCCESS) {
    displayText = c.CORRECT;
    classNames.push("correct");
  } else if (giveUp && type === c.USER && testResult === c.SUCCESS) {
    displayText = c.WHY;
    classNames.push("why");
  } else {
    displayText = c.INCORRECT;
    classNames.push("incorrect");
  }

  return (
    <div
      className={classNames.join(" ").trim()}
      style={{
        position: "absolute",
        left: leftPx,
        top: topPx,
        minHeight: minHeight,
        width: fdbkWidth,
      }}
    >
      {displayText}
    </div>
  );
};

export default Feedback;
