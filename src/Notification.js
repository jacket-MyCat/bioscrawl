import React from "react";
import PropTypes from "prop-types";

import "./Notification.css";
import * as c from "./constants.js";

export default class Notification extends React.Component {
  static propTypes = {
    message: PropTypes.string,
    msgType: PropTypes.string,
    id: PropTypes.string,
  };

  render() {
    const msg = this.props.message;
    const classNames = msg ? [c.MESSAGE, this.props.msgType] : [];
    const _id = this.props.id;

    return (
      <div id={_id} className="message-container">
        <div className={classNames.join(" ").trim()}>{msg}</div>
      </div>
    );
  }
}
