import React from "react";
import { isMobile } from "react-device-detect";

import Controller from './Controller.js';
import Modal from './Modal.js';
import * as c from "./constants.js";

import "./App.css";


function App() {

  if (isMobile) {
    return <div>Bioscrawl is not available on mobile devices</div>
  }

  return (
    <div className="App">
      <header className="App-header">
	<h1 className="App-title">Welcome to Bioscrawl</h1>
	<div className="App-picture"></div>
      </header>
      <Controller />
      <footer
	id={c.CANVAS_BOTTOM}
	className="App-footer"
      >
	<p className="App-license">Released under the <a
	  href="https://opensource.org/licenses/GPL-3.0">
	  GNU General Public License, version 3
	  </a>
	  <span className="App-date">
	    August 2020 | Michelle Echko | <a
	    href="https://bitbucket.org/jacket-MyCat/bioscrawl">
	    Code Repo
	    </a>
	  </span>
	</p>
      </footer>
    </div>
  );
}

export default App
