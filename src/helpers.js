const multiBondMath = (x1, y1, x2, y2, r, cx, w) => {
  // slope of  line passing through center
  // of both atoms
  const rise = y2 - y1;
  const run = x2 - x1;
  const m = rise / run;

  // slope of perpendicular line
  // is negative reciprocal
  const mp = -1 / m;

  // space between multiple bonds
  // is diameter of smaller atom
  // divided by # of bonds plus 1
  const space = (r * 2) / (w + 1);

  /* Starting from x coord directly
   * left of center on circumference of
   * smaller atom, iteratively
   * calculate distance between a given
   * bond and this center.
   *
   * Use distance and perpendicular slope
   * to get x and y coords for each bond.
   *
   * Add coords to an array and return
   * the array.
   *
   * cx is center x coord of smaller atom.
   */

  const bondArray = [];
  let newX1, newY1, newX2, newY2, dx, dy, distance;

  let start = cx - r;
  for (let i = 0; i < w; i++) {
    start = start + space;
    distance = start - cx;
    if (rise === 0) {
      // line passing through center
      // of both atoms is horizontal
      // making the perpendicular
      // line vertical with infinite
      // slope.
      // adjust y coords only.
      newX1 = x1;
      newY1 = y1 + distance;
      newX2 = x2;
      newY2 = y2 + distance;
    } else if (run === 0) {
      // line passing through center
      // of both atoms is vertical
      // making the perpendicular
      // line horizontal with slope 0.
      // adjust x coords only.
      newX1 = x1 + distance;
      newY1 = y1;
      newX2 = x2 + distance;
      newY2 = y2;
    } else {
      dx = distance / Math.sqrt(1 + mp * mp);
      dy = mp * dx;
      newX1 = x1 + dx;
      newY1 = y1 + dy;
      newX2 = x2 + dx;
      newY2 = y2 + dy;
    }

    bondArray.push({
      x1: newX1,
      y1: newY1,
      x2: newX2,
      y2: newY2,
    });
  }

  return bondArray;
};

export default multiBondMath;
