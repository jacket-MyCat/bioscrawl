import * as d3 from "d3";
import React from "react";

import D3Component from "./D3Component.js";
import Feedback from "./Feedback.js";
import * as c from "./constants.js";

const resize = (setDimensions) => {
  // calculte width
  const left = document.getElementById(c.CANVAS_LEFT).getBoundingClientRect()
    .right;
  const right = document.getElementById(c.CANVAS_RIGHT).getBoundingClientRect()
    .left;

  // calculate height
  const top = document.getElementById(c.CANVAS_TOP).getBoundingClientRect()
    .bottom;
  const bottom = document
    .getElementById(c.CANVAS_BOTTOM)
    .getBoundingClientRect().top;

  setDimensions({
    left: left,
    top: top,
    bottom: bottom,
    width: right - left,
    height: bottom - top,
  });
};

const SelectMode = (props) => {
  const nodes = props.nodes;
  const links = props.links;
  const canvasClick = props.canvasClick;
  const width = props.dimensions.width;
  const height = props.dimensions.height;

  const canvasRef = React.useRef(null);

  React.useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");

    const vis = new D3Component();
    vis.draw(nodes, links, context, width, height);

    let area = d3.select(canvas);
    area.on("mousedown", () => {
      updateMousePos(area);
    });
    area.on("mouseup", () => {
      fixMousePos(area);
    });

    const updateMousePos = (area) => {
      let point = d3.mouse(canvas);
      let [item, itemKey, idx] = nearestItem(point);
      if (!item) {
        return;
      }
      canvasClick(itemKey, idx);
      item.active = !item.active;
      area.on("mousemove", () => {
        if (itemKey === c.ATOMS) {
          let point = d3.mouse(canvas);
          item.x = point[0];
          item.y = point[1];
          vis.draw(nodes, links, context, width, height);
        }
      });
    };

    const fixMousePos = (area) => {
      area.on("mousemove", null);
      vis.draw(nodes, links, context, width, height);
    };

    const nearestItem = (point) => {
      let radius = 100,
        ll = links.length,
        i,
        dx,
        dy,
        d2,
        s2 = radius * radius,
        bond,
        circle,
        subject = [null, null, null];

      // check center of atoms
      for ([i, circle] of nodes.entries()) {
        dx = point[0] - circle.x;
        dy = point[1] - circle.y;
        d2 = dx * dx + dy * dy;
        if (d2 < s2) {
          subject = [circle, c.ATOMS, i];
          s2 = d2;
        }
      }

      // check midpoint of bonds
      for (i = 0; i < ll; i++) {
        bond = links[i];
        dx =
          point[0] -
          (nodes.get(bond.source).x +
            (nodes.get(bond.target).x - nodes.get(bond.source).x) / 2);
        dy =
          point[1] -
          (nodes.get(bond.source).y +
            (nodes.get(bond.target).y - nodes.get(bond.source).y) / 2);
        d2 = dx * dx + dy * dy;
        if (d2 < s2) {
          subject = [bond, c.BONDS, i];
          s2 = d2;
        }
      }

      return subject;
    };
  }, [nodes, links, width, height]);

  return <canvas ref={canvasRef} width={width} height={height} />;
};

const ForceSimulation = (props) => {
  // Format nodes and links for D3 forceSimulation
  const nodes =
    props.nodes instanceof Map ? Array.from(props.nodes.values()) : props.nodes;
  const links = props.links.map((l) => ({ ...l }));

  const width = props.dimensions.width;
  const height = props.dimensions.height;
  const leftPx = props.dimensions.left.toString() + "px";
  const topPx = props.dimensions.top.toString() + "px";

  const canvasRef = React.useRef(null);
  const vis = new D3Component();
  const simulation = vis.createSimulation(
    width,
    props.giveUp ? height - c.OFFSET : height
  );

  React.useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");

    vis.startSimWithDrag(
      canvas,
      context,
      nodes,
      links,
      simulation,
      width,
      height
    );

    return () => {
      vis.stopSimAndDrag(canvas, simulation);
    };
  }, [nodes, links, width, height]);

  return (
    <canvas
      ref={canvasRef}
      width={width}
      height={height}
      style={{
        position: "absolute",
        left: leftPx,
        top: topPx,
      }}
    />
  );
};

const transformCoordinates = (dimensions, answer) => {
  // Find min-max coords of molecule
  let minX = Number.MAX_VALUE,
    maxX = Number.MIN_VALUE,
    minY = Number.MAX_VALUE,
    maxY = Number.MIN_VALUE;

  answer.nodes.forEach((n) => {
    minX = Math.min(n.x, minX);
    maxX = Math.max(n.x, maxX);
    minY = Math.min(n.y, minY);
    maxY = Math.max(n.y, maxY);
  });

  // calculate scalar
  const wPubChem = Math.abs(maxX - minX);
  const hPubChem = Math.abs(maxY - minY);

  const scalar = Math.min(
    dimensions.width / wPubChem,
    dimensions.height / hPubChem
  );

  const center = {
    x: wPubChem / 2,
    y: hPubChem / 2,
  };

  // scale molecule
  answer.nodes.forEach((n) => {
    n.x = (n.x - center.x) * scalar;
    n.y = (n.y - center.y) * scalar * -1;
  });
};

const CanvasActivity = (props) => {
  const isSelectMode = props.isSelectMode;
  const zero = { left: 0, top: 0, bottom: 0, width: 0, height: 0 };
  const [dims, setDimensions] = React.useState(zero);
  const ref = React.useRef(null);

  React.useEffect(() => {
    resize(setDimensions);
  }, []);

  const handleResize = () => {
    resize(setDimensions);
  };

  React.useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  });

  if (isSelectMode) {
    return (
      <div className="component-canvas" ref={ref}>
        <SelectMode
          dimensions={dims}
          nodes={props.nodes}
          links={props.links}
          canvasClick={props.canvasClick}
        />
      </div>
    );
  }

  const answer = props.answer;
  let answerDims, userDims;
  if (props.giveUp) {
    /* split dimensions in half */
    userDims = {
      left: dims.left,
      top: dims.top,
      bottom: dims.bottom,
      width: dims.width / 2,
      height: dims.height,
    };
    answerDims = {
      left: dims.left + dims.width / 2,
      top: dims.top,
      bottom: dims.bottom,
      width: dims.width / 2,
      height: dims.height,
    };

    transformCoordinates(answerDims, answer);
  } else {
    /* assign full dimensions to user variable */
    userDims = dims;
    answerDims = zero;
  }

  return (
    <>
      <div key="user" className="component-canvas" ref={ref}>
        <ForceSimulation
          dimensions={userDims}
          nodes={props.nodes}
          links={props.links}
          giveUp={props.giveUp}
        />
        <Feedback
          dimensions={userDims}
          giveUp={props.giveUp}
          testResult={props.testResult}
          type={c.USER}
        />
      </div>
      <div key="answer" className="component-canvas" ref={ref}>
        <ForceSimulation
          dimensions={answerDims}
          nodes={answer.nodes}
          links={answer.links}
          giveUp={props.giveUp}
        />
        <Feedback
          dimensions={answerDims}
          giveUp={props.giveUp}
          testResult={props.testResult}
          type={c.ANSWER}
        />
      </div>
    </>
  );
};

export default CanvasActivity;
