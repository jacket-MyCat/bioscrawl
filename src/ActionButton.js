import React from "react";
import PropTypes from "prop-types";
import "./ActionButton.css";

import * as c from "./constants.js";

export default class ActionButton extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    direction: PropTypes.string,
    selectMode: PropTypes.bool,
    giveUp: PropTypes.bool,
    testResult: PropTypes.string,
    clickHandler: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.disableOnSelect = [c.UNDO, c.REDO, c.TEST, c.I_GIVE_UP, c.START_OVER];
    this.disableOnSimulation = [c.BOND, c.DELETE];
  }

  handleClick = () => {
    this.props.clickHandler(this.props.name);
  };

  render() {
    const selectMode = this.props.selectMode;
    const giveUp = this.props.giveUp;
    const testResult = this.props.testResult;
    const direction = this.props.direction;
    const disableOnSelect = this.disableOnSelect;
    const disableOnSimulation = this.disableOnSimulation;

    const classNames = [
      "action",
      direction,
      (giveUp && this.props.name !== c.START_OVER) ||
      (testResult === c.SUCCESS && this.props.name !== c.START_OVER) ||
      (selectMode && disableOnSelect.includes(this.props.name)) ||
      (!selectMode && disableOnSimulation.includes(this.props.name))
        ? "disabled"
        : "",
      selectMode && this.props.name === c.SELECT ? "select-mode-on" : "",
    ];

    return (
      <button
        className={classNames.join(" ").trim()}
        onClick={this.handleClick}
      >
        {this.props.name}
      </button>
    );
  }
}
