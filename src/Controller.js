import React from "react";

import AtomPanel from "./AtomPanel.js";
import ActionPanelLeft from "./ActionPanelLeft.js";
import ActionPanelTop from "./ActionPanelTop.js";
import CanvasActivity from "./CanvasActivity.js";
import Notification from "./Notification.js";

import atoms from "./atoms.json";
import * as c from "./constants.js";

import "./Controller.css";

export default class Game extends React.Component {
  constructor(props) {
    super(props);

    this.atoms = atoms;

    let initialState = this.getInitialState();
    initialState.moleculeNames = null;
    this.state = initialState;
  }

  getInitialState = () => {
    return {
      history: [
        {
          nodes: new Map(),
          links: [],
          adjacency: new Map(),
          maxAtomID: 0,
        },
      ],
      stepNumber: 0,
      selectMode: false,
      active: {
        atoms: new Set(),
        bonds: new Set(),
      },
      objective: "",
      statusMsg: c.MSG_PICK,
      errorMsg: "",
      giveUp: false,
      testResult: c.NA,
      answer: {
        nodes: [],
        links: [],
      },
    };
  };

  componentDidMount() {
    window.addEventListener("unhandledrejection", function (event) {
      alert(event.promise);
      alert(event.reason);
    });

    fetch("/api/answers")
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw Error(response.statusText);
        }
      })
      .then((data) => {
        this.setState({
          moleculeNames: data,
        });
      })
      .catch((error) => {
        alert(error);
      });
  }

  handleActionClick = (buttonName) => {
    switch (buttonName) {
      case c.BOND:
        this.handleBondClick();
        break;
      case c.DELETE:
        this.handleDeleteClick();
        break;
      case c.SELECT:
        this.handleSelectClick();
        break;
      case c.START_OVER:
        this.handleStartOver();
        break;
      case c.UNDO:
        this.handleUndoClick();
        break;
      case c.REDO:
        this.handleRedoClick();
        break;
      case c.TEST:
        this.handleTestClick();
        break;
      case c.I_GIVE_UP:
        this.handleGiveUpClick();
        break;
      default:
        break;
    }
  };

  deepCopyMolecule = (current) => {
    let newNodes = new Map();
    for (let [key, value] of current.nodes.entries()) {
      newNodes.set(key, { ...value });
    }
    let newLinks = current.links.map((l) => ({ ...l }));
    let newAdjacency = new Map();
    for (let [key, value] of current.adjacency.entries()) {
      newAdjacency.set(key, new Map(value));
    }

    return [newNodes, newLinks, newAdjacency];
  };

  handleBondClick = () => {
    if (
      !this.state.selectMode ||
      this.state.giveUp ||
      this.state.testResult === c.SUCCESS
    ) {
      return;
    }

    let activeLength = this.state.active[c.ATOMS].size;
    if (activeLength !== 2) {
      this.setState({
        errorMsg:
          " Bonding requires 2 selected" +
          " atoms. Number of atoms currently " +
          " selected: " +
          activeLength.toString() +
          "." +
          " Change the number of selected " +
          " atoms and try again. ",
      });
      return;
    }
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];

    // return if bond is invalid
    if (!this.isValidBond(current)) {
      return;
    }

    // make deep copy of molecule information to
    // update state/history
    let [newNodes, newLinks, newAdjacency] = this.deepCopyMolecule(current);

    const [firstAtom, secondAtom] = this.state.active[c.ATOMS];
    const neighbors = current.adjacency.get(firstAtom);

    if (neighbors.has(secondAtom)) {
      // atoms are already bonded, increase
      // bond number by 1
      let linkIndex = current.adjacency.get(firstAtom).get(secondAtom);
      newLinks[linkIndex].weight++;
    } else {
      // atoms are not bonded, update
      // links array and adjacency array
      // accordingly
      newLinks = newLinks.concat([
        {
          source: firstAtom,
          target: secondAtom,
          weight: 1,
        },
      ]);
      let linkIndex = newLinks.length - 1;
      newAdjacency.get(firstAtom).set(secondAtom, linkIndex);
      newAdjacency.get(secondAtom).set(firstAtom, linkIndex);
    }
    // clear current active items so they are
    // stored as inactive in history
    this.clearHighlighted(current);

    //update state
    this.setState({
      history: history.concat([
        {
          nodes: newNodes,
          links: newLinks,
          adjacency: newAdjacency,
          maxAtomID: current.maxAtomID,
        },
      ]),
      stepNumber: history.length,
      errorMsg: "",
    });
  };

  isValidBond = (current) => {
    // use for loop instead of forEach.
    // immediate exit from function
    // is required if an invalid bond
    // is found.
    let activeAtom;
    for (activeAtom of this.state.active[c.ATOMS].values()) {
      let adjacency = current.adjacency.get(activeAtom);
      let numBonds = 0;
      for (let i of adjacency.values()) {
        numBonds = numBonds + current.links[i].weight;
      }
      let maxBonds = this.atoms[current.nodes.get(activeAtom).symbol].covalence;
      if (numBonds === maxBonds) {
        this.setState({
          errorMsg:
            " The " +
            current.nodes.get(activeAtom).name +
            " atom already has " +
            numBonds.toString() +
            " covalent bond(s) and cannot share" +
            " another electron. ",
        });
        return false;
      }
    }
    this.setState({ errorMsg: "" });
    return true;
  };

  handleDeleteClick = () => {
    if (
      !this.state.selectMode ||
      this.state.giveUp ||
      this.state.testResult === c.SUCCESS ||
      (this.state.active.atoms.size === 0 && this.state.active.bonds.size === 0)
    ) {
      return;
    }

    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    let maxAtomID = current.maxAtomID;

    // Make deep copy of molecule information to
    // update state/history
    let [newNodes, newLinks, newAdjacency] = this.deepCopyMolecule(current);

    // Delete active nodes from node and adjacency Maps
    // and reduce associated bond weight(s)
    // to 1. Add associated bond indices to active bond set
    // for link deletion later.
    const newActiveAtoms = Array.from(this.state.active.atoms);
    const newActiveBonds = new Set(this.state.active.bonds);
    while (newActiveAtoms.length) {
      let firstAtom = newActiveAtoms.pop();
      newNodes.delete(firstAtom);
      let neighbors = newAdjacency.get(firstAtom);
      for (let [secondAtom, linkIndex] of neighbors.entries()) {
        newLinks[linkIndex].weight = 1;
        if (!newActiveBonds.has(linkIndex)) {
          newActiveBonds.add(linkIndex);
        }
        newAdjacency.get(secondAtom).delete(firstAtom);
      }
      newAdjacency.delete(firstAtom);
      maxAtomID = firstAtom === maxAtomID ? maxAtomID - 1 : maxAtomID;
    }

    // Remove active bonds in descending index order.
    // Any bond with weight greater than 1
    // was not connected to a node deleted above.
    // Reduce the bond weight and temporarily
    // store it until link indices are finalized and
    // the new active bond set can be created.
    // Remove links from adjacency Map as needed.
    const sortedBonds = Array.from(newActiveBonds).sort(function (a, b) {
      return a - b;
    });
    newActiveBonds.clear();
    let temp = [];
    const minLinkIndex = sortedBonds[0];
    while (sortedBonds.length) {
      let i = sortedBonds.pop();
      if (newLinks[i].weight > 1) {
        newLinks[i].weight--;
        temp = temp.concat(newLinks.splice(i, 1));
      } else {
        let link = newLinks.splice(i, 1)[0];
        if (newAdjacency.get(link.source)) {
          newAdjacency.get(link.source).delete(link.target);
        }
        if (newAdjacency.get(link.target)) {
          newAdjacency.get(link.target).delete(link.source);
        }
      }
    }

    // Create new active bond set with
    // stable link indices
    for (let i = 0; i < temp.length; i++) {
      newActiveBonds.add(newLinks.length + i);
    }
    newLinks = newLinks.concat(temp);
    temp = null;

    // Rebuild adjacency matrix starting from minimum
    // link index removed since all indices that
    // come after it are stale now.
    for (let i = minLinkIndex; i < newLinks.length; i++) {
      let s = newLinks[i].source;
      let t = newLinks[i].target;
      newAdjacency.get(s).set(t, i);
      newAdjacency.get(t).set(s, i);
    }

    // clear current active items so they are
    // stored as inactive in history
    this.clearHighlighted(current);

    // Update state
    this.setState({
      history: history.concat([
        {
          nodes: newNodes,
          links: newLinks,
          adjacency: newAdjacency,
          maxAtomID: maxAtomID,
        },
      ]),
      stepNumber: history.length,
      errorMsg: "",
      active: {
        atoms: new Set(newActiveAtoms),
        bonds: newActiveBonds,
      },
    });
  };

  handleSelectClick = () => {
    if (this.state.giveUp || this.state.testResult === c.SUCCESS) {
      return;
    }

    const newSelectMode = !this.state.selectMode;
    this.setState({
      selectMode: newSelectMode,
      errorMsg: "",
      testResult: c.NA,
    });
    if (!newSelectMode) {
      // selectMode is turned off
      // clean up active components
      this.clearHighlighted();
      this.clearActiveState();
    }
  };

  clearHighlighted = (current) => {
    if (!current) {
      let history = this.state.history.slice(0, this.state.stepNumber + 1);
      current = history[history.length - 1];
    }

    this.state.active[c.ATOMS].forEach((i) => {
      current.nodes.get(i).active = false;
    });
    this.state.active[c.BONDS].forEach((i) => {
      current.links[i].active = false;
    });
  };

  clearActiveState = () => {
    this.setState({
      active: {
        atoms: new Set(),
        bonds: new Set(),
      },
    });
  };

  handleUndoClick = () => {
    const step = this.state.stepNumber - 1;
    if (
      !this.state.giveUp &&
      !this.state.selectMode &&
      this.state.testResult !== c.SUCCESS &&
      step >= 0
    ) {
      this.jumpTo(step);
    }
    return;
  };

  handleRedoClick = () => {
    const step = this.state.stepNumber + 1;
    if (
      !this.state.giveUp &&
      !this.state.selectMode &&
      this.state.testResult !== c.SUCCESS &&
      step <= this.state.history.length - 1
    ) {
      this.jumpTo(step);
    }
    return;
  };

  handleRegistration = (molecule) => {
    if (!molecule) {
      this.setState({
        errorMsg: c.MSG_REGISTER,
      });
    } else {
      const statusMsg = c.MSG_DRAW.replace("*", molecule);
      this.setState({
        objective: molecule,
        statusMsg: statusMsg,
        errorMsg: "",
        testResult: c.NA,
      });
    }
  };

  handleStartOver = () => {
    if (this.state.selectMode || !this.state.objective.length) {
      return;
    }

    const objective = this.state.objective;
    const statusMsg = c.MSG_DRAW.replace("*", objective);
    let newState = this.getInitialState();
    newState.objective = objective;
    newState.statusMsg = statusMsg;
    this.setState(newState);
  };

  handleCanvasClick = (itemKey, idx) => {
    const isActive = this.state.active[itemKey].has(idx);
    const newActive = this.state.active;

    if (isActive) {
      // item is in the active set, remove it
      newActive[itemKey].delete(idx);
    } else {
      // item is not in the active set, add it
      newActive[itemKey].add(idx);
    }

    this.setState({
      active: newActive,
      errorMsg: "",
    });
  };

  handleAtomClick = (buttonName) => {
    if (
      this.state.selectMode ||
      this.state.giveUp ||
      this.state.testResult === c.SUCCESS
    ) {
      return;
    }

    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const maxAtomID = current.maxAtomID + 1;
    const [newNodes, newLinks, newAdjacency] = this.deepCopyMolecule(current);

    const atom = { ...this.atoms[buttonName] };
    atom.id = maxAtomID;
    atom.foci = 0;

    this.setState({
      history: history.concat([
        {
          nodes: newNodes.set(maxAtomID, atom),
          links: newLinks,
          adjacency: newAdjacency.set(maxAtomID, new Map()),
          maxAtomID: maxAtomID,
        },
      ]),
      stepNumber: history.length,
      errorMsg: "",
      testResult: c.NA,
    });
  };

  testUserMolecule = async () => {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const userData = {
      objective: this.state.objective,
      molecule: {
        nodes: Array.from(current.nodes.values()),
        links: current.links,
      },
    };
    const response = await fetch("/api/compare", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userData),
    });

    return response;
  };

  handleTestClick = async () => {
    if (
      !this.state.objective.length ||
      this.state.selectMode ||
      this.state.giveUp ||
      this.state.testResult === c.SUCCESS
    ) {
      return;
    }

    const response = await this.testUserMolecule();
    if (!response.ok) {
      throw Error(response.statusText);
    }
    const isSame = await response.json();

    const newState = isSame.result
      ? {
          statusMsg: c.MSG_CORRECT,
          errorMsg: "",
          testResult: c.SUCCESS,
        }
      : {
          statusMsg: c.MSG_DRAW.replace("*", this.state.objective),
          errorMsg: c.MSG_INCORRECT,
          testResult: c.FAIL,
        };
    this.setState(newState);
  };

  handleGiveUpClick = async () => {
    if (
      !this.state.objective.length ||
      this.state.selectMode ||
      this.state.giveUp ||
      this.state.testResult === c.SUCCESS
    ) {
      return;
    }

    // Test user molecule
    let response = await this.testUserMolecule();
    if (!response.ok) {
      throw Error(response.statusText);
    }
    const isCorrect = (await response.json()).result;

    const userData = {
      objective: this.state.objective,
    };
    response = await fetch("/api/give_up", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userData),
    });

    if (!response.ok) {
      throw Error(response.statusText);
    }
    const responseJSON = await response.json();
    const solution = responseJSON[this.state.objective];

    solution.nodes.forEach((n) => {
      n.size = this.atoms[n.symbol].size;
      n.color = this.atoms[n.symbol].color;
    });

    this.setState({
      statusMsg: c.MSG_START_OVER,
      errorMsg: "",
      giveUp: true,
      testResult: isCorrect ? c.SUCCESS : c.FAIL,
      answer: {
        nodes: solution.nodes,
        links: solution.links,
      },
    });
  };

  jumpTo(step) {
    this.setState({
      stepNumber: step,
    });
  }

  render() {
    if (!this.state.moleculeNames) {
      return <div> Loading ... </div>;
    }

    const names = this.state.moleculeNames;
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const selectMode = this.state.selectMode;
    const objective = this.state.objective;
    const statusMsg = this.state.statusMsg;
    const errorMsg = this.state.errorMsg;
    const giveUp = this.state.giveUp;
    const testResult = this.state.testResult;
    const answer = this.state.answer;

    return (
      <div className="container-game">
        <ActionPanelTop
          selectMode={selectMode}
          giveUp={giveUp}
          testResult={testResult}
          answers={names}
          objective={objective}
          clickHandler={this.handleActionClick}
          onSubmit={this.handleRegistration}
        />
        <div className="container-middle">
          <ActionPanelLeft
            selectMode={selectMode}
            giveUp={giveUp}
            testResult={testResult}
            clickHandler={this.handleActionClick}
          />
          <AtomPanel
            selectMode={selectMode}
            giveUp={giveUp}
            testResult={testResult}
            atomInfo={this.atoms}
            clickHandler={this.handleAtomClick}
          />
          <div className="container-center">
            <Notification
              message={statusMsg}
              msgType={c.STATUS}
              id={c.STATUS}
            />
            <Notification
              message={errorMsg}
              msgType={c.ERROR}
              id={c.CANVAS_TOP}
            />
            <CanvasActivity
              isSelectMode={selectMode}
              nodes={current.nodes}
              links={current.links}
              giveUp={giveUp}
              testResult={testResult}
              answer={answer}
              canvasClick={selectMode ? this.handleCanvasClick : null}
            />
          </div>
        </div>
      </div>
    );
  }
}
