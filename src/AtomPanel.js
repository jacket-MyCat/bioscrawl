import AtomButton from "./AtomButton";
import React from "react";
import PropTypes from "prop-types";

import "./AtomPanel.css";
import * as c from "./constants.js";

export default class AtomPanel extends React.Component {
  static propTypes = {
    selectMode: PropTypes.bool,
    giveUp: PropTypes.bool,
    testResult: PropTypes.string,
    atomInfo: PropTypes.object,
    clickHandler: PropTypes.func,
  };

  handleClick = (buttonName) => {
    this.props.clickHandler(buttonName);
  };

  renderAtomPanel() {
    const atomVals = Object.values(this.props.atomInfo);
    const selectMode = this.props.selectMode;
    const giveUp = this.props.giveUp;
    const testResult = this.props.testResult;

    let atomButtons = [];
    for (let i = 0; i < atomVals.length; i++) {
      if (i < atomVals.length) {
        atomButtons.push(
          <span key={"atom-index-" + i.toString()}>
            <AtomButton
              name={atomVals[i].symbol}
              color={atomVals[i].color}
              selectMode={selectMode}
              giveUp={giveUp}
              testResult={testResult}
              clickHandler={this.handleClick}
            />
          </span>
        );
      } else {
        break;
      }
    }

    return <div>{atomButtons}</div>;
  }

  render() {
    return (
      <div id={c.CANVAS_RIGHT} className="component-atom-panel">
        {this.renderAtomPanel()}
      </div>
    );
  }
}
