import React from "react";
import PropTypes from "prop-types";

import ActionButton from "./ActionButton";
import Modal from "./Modal.js";

import "./ActionPanel.css";
import * as c from "./constants.js";

const ActionPanelTop = (props) => {
  /* Molecule selector, Test, I-Give-Up, Start-Over,
   * and Instructions are towards top of screen under
   * "Welcome to Bioscrawl" banner.
   */
  const direction = "top";
  const [molecule, setMolecule] = React.useState("");
  const modalRef = React.useRef();

  const openModal = () => {
    modalRef.current.openModal();
  };

  const handleChange = (event) => {
    if (event.target.value !== props.objective) {
      setMolecule(event.target.value);
      props.onSubmit(event.target.value);
    }
  };

  const handleClick = (buttonName) => {
    props.clickHandler(buttonName);
  };

  const renderForm = () => {
    const answers = props.answers;
    const otherOptions = [];

    answers.forEach((o) => {
      otherOptions.push(
        <option key={o} value={o}>
          {o}
        </option>
      );
    });

    return (
      <div>
        <form>
          <label htmlFor="molSelect">Pick a Molecule:</label>
          <select
            id="molSelect"
            name="molecule"
            type="submit"
            onChange={handleChange}
            value={molecule || "default"}
          >
            <option disabled value="default">
              choose
            </option>
            {otherOptions}
          </select>
        </form>
      </div>
    );
  };

  const renderActions = (direction) => {
    const selectMode = props.selectMode;
    const giveUp = props.giveUp;
    const testResult = props.testResult;
    const keyText = `action-${direction}-`;
    const actions = [c.TEST, c.START_OVER, c.I_GIVE_UP];

    let actionButtons = [];
    for (let i = 0; i < actions.length; i++) {
      actionButtons.push(
        <span key={keyText + i.toString()}>
          <ActionButton
            name={actions[i]}
            direction={direction}
            selectMode={selectMode}
            giveUp={giveUp}
            testResult={testResult}
            clickHandler={handleClick}
          />
        </span>
      );
    }
    actionButtons.push(
      <>
        <button className="action top" onClick={openModal}>
          Instructions
        </button>
        <Modal ref={modalRef}>
          <h1>How to play</h1>
          <br></br>
          <ul>
            <li>
              <span className="bullet">Pick a molecule</span> - Click on{" "}
              <span className="choose">choose</span> to reveal a list of
              available options. Move your cursor over the name of the molecule
              you want to draw and click to select it. The text in the{" "}
              <span className="status instructions">status</span> box will
              change to reflect your choice.
            </li>
            <li>
              <span className="bullet">Add atoms</span> - Bring atoms onto the
              board by clicking the element symbols on the right-hand side of
              the screen ("C", "H", "N", "O", "P", "S").
            </li>
            <li>
              <span className="bullet">Bond atoms</span> - Select, Highlight,
              Bond.
              <ul>
                <li>
                  Click the <span className="action instructions">Select</span>{" "}
                  button to enter select mode. The color of the button changes
                  when select mode is turned on (
                  <span className="select-mode-on instructions">Select</span>
                  ).
                </li>
                <li>
                  With select mode on, move your cursor over an atom and click
                  once to highlight it. Click again to remove the highlight.
                </li>
                <li>
                  When exactly two atoms are highlighted, click the{" "}
                  <span className="action instructions">Bond</span> button to
                  link them together. Click it again to create multiple bonds
                  between highlighted atoms.
                </li>
                <li>
                  {" "}
                  Pay attention to{" "}
                  <span className="error instructions">error</span> messages
                  that pop up to guide you.
                </li>
              </ul>
            </li>
            <li>
              <span className="bullet">Delete things</span> - Select, Highlight,
              Delete.
              <ul>
                <li>Enter select mode same as before.</li>
                <li>
                  With select mode on, move your cursor over an atom{" "}
                  <span style={{ fontWeight: 550 }}>or a bond</span> and click
                  once to highlight it. Multiple items can be highlighted in
                  this way. Click on an item again to remove its highlight.
                </li>
                <li>
                  Click the <span className="action instructions">Delete</span>{" "}
                  button to remove all highlighted items from the board.
                </li>
              </ul>
            </li>
            <li>
              <span className="bullet">Check your work</span>
              <ul>
                <li>
                  To find out if your drawing is{" "}
                  <span className="correct instructions">Correct</span> or{" "}
                  <span className="incorrect instructions">Incorrect</span>,
                  turn off select mode and use the{" "}
                  <span className="action instructions">Test</span> button.
                </li>
                <li>
                  If you've had enough and want to see the solution, turn off
                  select mode and click the{" "}
                  <span className="action instructions">I Give Up!</span>{" "}
                  button.
                </li>
              </ul>
            </li>
            <li>
              <span className="bullet">Time Travel</span>
              <ul>
                <li>
                  Go backward or forward in your action history with the{" "}
                  <span className="action instructions">Undo</span> and{" "}
                  <span className="action instructions">Redo</span> buttons,
                  respectively.
                </li>
                <li>
                  Erase your action history completely and start from the
                  beginning with the{" "}
                  <span className="action instructions">Start Over</span>{" "}
                  button.
                </li>
              </ul>
            </li>
          </ul>
          <br></br>
          <button onClick={() => modalRef.current.close()}>Close</button>
        </Modal>
      </>
    );

    return (
      <>
        {renderForm()}
        <div>{actionButtons}</div>
      </>
    );
  };

  return (
    <div className={`component-actions-${direction}`}>
      {renderActions(direction)}
    </div>
  );
};

ActionPanelTop.propTypes = {
  selectMode: PropTypes.bool,
  giveUp: PropTypes.bool,
  testResult: PropTypes.string,
  answers: PropTypes.array,
  objective: PropTypes.string,
  clickHandler: PropTypes.func,
  onSubmit: PropTypes.func,
};

export default ActionPanelTop;
