export const ANSWER = "answer";
export const ATOMS = "atoms";
export const BOND = "Bond";
export const BONDS = "bonds";
export const CANVAS_BOTTOM = "bottom";
export const CANVAS_LEFT = "left";
export const CANVAS_RIGHT = "right";
export const CANVAS_TOP = "top";
export const CORRECT = "Correct";
export const DELETE = "Delete";
export const ERROR = "error";
export const FAIL = "fail";
export const I_GIVE_UP = "I Give Up!";
export const INCORRECT = "Incorrect";
export const MESSAGE = "message";
export const MSG_CORRECT =
  'Great work! Click "Start Over" to practice ' +
  "again or pick a new molecule.";
export const MSG_DRAW = "Drawing *...";
export const MSG_INCORRECT = "Keep working on it, you can do it!";
export const MSG_PICK = "Pick a molecule to draw";
export const MSG_REGISTER = "Register a molecule using the drop down menu";
export const MSG_START_OVER =
  "Drag atoms to separate any overlapping bonds. " +
  'Click "Start Over" to give it another shot.';
export const NA = "not applicable";
export const OFFSET = 40;
export const REDO = "Redo";
export const SELECT = "Select";
export const START_OVER = "Start Over";
export const STATUS = "status";
export const SUCCESS = "success";
export const TEST = "Test";
export const UNDO = "Undo";
export const USER = "user";
export const WHY = "You got it right. Why did you give up?";
