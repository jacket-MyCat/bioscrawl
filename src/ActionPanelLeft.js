import ActionButton from "./ActionButton";
import React from "react";
import PropTypes from "prop-types";

import "./ActionPanel.css";
import * as c from "./constants.js";

export default class ActionPanelLeft extends React.Component {
  static propTypes = {
    selectMode: PropTypes.bool,
    giveUp: PropTypes.bool,
    testResult: PropTypes.string,
    clickHandler: PropTypes.func,
  };

  handleClick = (buttonName) => {
    this.props.clickHandler(buttonName);
  };

  renderActions = (direction) => {
    const selectMode = this.props.selectMode;
    const giveUp = this.props.giveUp;
    const testResult = this.props.testResult;
    const keyText = `action-${direction}-`;
    const actions = [c.SELECT, c.BOND, c.DELETE, c.UNDO, c.REDO];

    let actionButtons = [];
    for (let i = 0; i < actions.length; i++) {
      actionButtons.push(
        <span key={keyText + i.toString()}>
          <ActionButton
            name={actions[i]}
            direction={direction}
            selectMode={selectMode}
            giveUp={giveUp}
            testResult={testResult}
            clickHandler={this.handleClick}
          />
        </span>
      );
    }

    return <div>{actionButtons}</div>;
  };

  render() {
    /* Select, Bond, Delete, and Redo buttons are on left hand
     * side of screen.
     */

    const direction = "left";

    return (
      <>
        <div id={c.CANVAS_LEFT} className={`component-actions-${direction}`}>
          {this.renderActions(direction)}
        </div>
      </>
    );
  }
}
