import React from "react";
import PropTypes from "prop-types";
import "./AtomButton.css";

import * as c from "./constants.js";

export default class AtomButton extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    color: PropTypes.string,
    selectMode: PropTypes.bool,
    giveUp: PropTypes.bool,
    testResult: PropTypes.string,
    clickHandler: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      hover: false,
    };
  }

  mouseOver = () => {
    this.setState({ hover: true });
  };

  mouseOut = () => {
    this.setState({ hover: false });
  };

  handleClick = () => {
    this.props.clickHandler(this.props.name);
  };

  render() {
    const isHydrogen = this.props.name === "H";
    const selectMode = this.props.selectMode;
    const giveUp = this.props.giveUp;
    const testResult = this.props.testResult;
    const classNames = [
      "atom",
      selectMode || giveUp || testResult === c.SUCCESS ? "disabled" : "",
    ];

    return (
      <button
        style={{
          color: this.state.hover ? "white" : "black",
          backgroundColor: this.state.hover
            ? isHydrogen
              ? "black"
              : this.props.color
            : "white",
        }}
        className={classNames.join(" ").trim()}
        onMouseOver={this.mouseOver}
        onMouseOut={this.mouseOut}
        onClick={this.handleClick}
      >
        {this.props.name}
      </button>
    );
  }
}
