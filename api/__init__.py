from flask import Flask
from .answers import Answers
from .molecule import Molecule
from .constants import Graph, Mol, PubChem


A = Answers()

def create_app():
    app = Flask(__name__)

    from .views import main
    app.register_blueprint(main)

    return app
