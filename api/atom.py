from dataclasses import dataclass
from os import path
import json
from typing import List

@dataclass
class Element:
    name: str
    symbol: str
    covalence: int
    color: str
    size: int

        
class Atom:
    def __init__(self):
        self.carbon = Element("Carbon", "C", 4, "#95A5A6", 10)
        self.hydrogen = Element("Hydrogen", "H", 1, "white", 5)
        self.oxygen = Element("Oxygen", "O", 2, "#E74C3C", 12)
        self.nitrogen = Element("Nitrogen", "N", 3, "#2980B9", 11)
        self.phosphorus = Element("Phosphorus", "P", 5, "#E67E22", 13)
        self.sulphur = Element("Sulphur", "S", 6, "#F1C40F", 14)
    
    def get_atoms(self) -> List[Element]:
        return [
	    self.carbon, self.hydrogen, self.oxygen,
            self.nitrogen, self.phosphorus, self.sulphur
        ]
    
    def export_json(self) -> None:
        config = {}
        for atom in self.get_atoms():
            config.update({atom.symbol: atom.__dict__})

        here = path.dirname(path.realpath(__file__))
        with open(path.join(here, "atoms.json"), "w") as outfile:
            json.dump(config, outfile, sort_keys=True, indent=2)

