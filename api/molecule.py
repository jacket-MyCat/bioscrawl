import networkx as nx
import networkx.algorithms.isomorphism as iso
from networkx.readwrite import json_graph

from .constants import Graph

class Molecule:
    def __init__(self, name=None, node_link=None):
        self.name = name
        if (node_link is None):
            self.G = nx.Graph()
        else:
            node_link.update(Graph.NX_GRAPH_DEFAULTS)
            self.G = json_graph.node_link_graph(node_link)

    def add_atom(self, atomID, symbol):
        self.G.add_node(atomID, symbol=symbol)
 
    def bond(self, u, v, num_bonds):
        self.G.add_edge(u, v, weight=num_bonds)

    def to_node_link(self):
        node_link = json_graph.node_link_data(self.G)
        name = self.name
        molecule = {
            name: {
                Graph.NODES: node_link[Graph.NODES],
                Graph.LINKS: node_link[Graph.LINKS]
            }
        }

        return molecule
    
    def is_same(self, other):
        nm = iso.categorical_node_match(
            Graph.SYMBOL,
            Graph.DEFAULT_SYMBOL
        )
        em = iso.numerical_edge_match(Graph.WEIGHT, 1.0)
        return nx.is_isomorphic(
            self.G,
            other.G,
            node_match=nm,
            edge_match=em
        )
      
