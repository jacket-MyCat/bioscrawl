class Graph:
    DEFAULT_SYMBOL = "X"
    DIRECTED = "directed"
    GRAPH = "graph"
    ID = "id"
    LINKS = "links"
    MULTIGRAPH = "multigraph"
    NODES = "nodes"
    SOURCE = "source"
    SYMBOL = "symbol"
    TARGET ="target"
    WEIGHT = "weight"

    NX_GRAPH_DEFAULTS = {
        DIRECTED: False,
        MULTIGRAPH: True,
        GRAPH: {}
    }


class Mol:
    class Name:
        ADENINE = "Adenine"
        ALANINE = "Alanine"
        BETA_D_RIBOFURANOSE = "Beta-D-Ribofuranose"
        CYSTEINE = "Cysteine"
        CYCLIC_GLUCOSE = "Cyclic Glucose"
        DEOXYRIBOSE_5_PHOSPHATE = "Deoxyribose 5-phosphate"
        GLYCINE = "Glycine"
        METHANE = "Methane"
        WATER = "Water"

        ALL = [
            ADENINE, ALANINE, BETA_D_RIBOFURANOSE,
            CYSTEINE, CYCLIC_GLUCOSE,
            DEOXYRIBOSE_5_PHOSPHATE, GLYCINE,
            METHANE, WATER
        ]

class PubChem:
    AID = "aid"
    AID1 = "aid1"
    AID2 = "aid2"
    ATOMS = "atoms"
    BONDS = "bonds"
    ELEMENT = "element"
    ORDER = "order"

    class CID:
        ADENINE = 190
        ALANINE = 5950
        BETA_D_RIBOFURANOSE = 447347
        CYSTEINE = 5862
        CYCLIC_GLUCOSE = 5793
        DEOXYRIBOSE_5_PHOSPHATE = 45934311
        GLYCINE = 750
        METHANE = 297
        RIBOSE = 10975657
        WATER = 962

        ALL = [
            ADENINE, ALANINE, BETA_D_RIBOFURANOSE,
            CYSTEINE, CYCLIC_GLUCOSE,
            DEOXYRIBOSE_5_PHOSPHATE, GLYCINE,
            METHANE, WATER
        ]
