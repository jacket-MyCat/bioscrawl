from os import path
import json
import logging
from typing import List

import networkx as nx
from networkx.readwrite import json_graph

from .molecule import Molecule
from .constants import Graph, Mol, PubChem

logging.basicConfig(level=logging.DEBUG)

class Answers:
    @staticmethod
    def get_names() -> List[str]:
        return Mol.Name.ALL

    def get_answer(self, name) -> Molecule:
        return self._make_molecule(name)
     
    def export_all(self) -> None:
        answer_dict = {}
        for molecule in self.get_names():
            logging.info(f"Reading in json for {molecule}")
            M = self._make_molecule(molecule)
            answer_dict.update({M.name: M.to_node_link()})

        here = path.dirname(path.realpath(__file__))
        with open(path.join(here, "answers.json"), "w") as outfile:
            json.dump(
                answer_dict,
                outfile,
                sort_keys=True,
                indent=2
            )

    def retrieve_from_pubchem(self) -> None:
        from pubchempy import Compound
        for name, cid in zip(Mol.Name.ALL, PubChem.CID.ALL):
            logging.info(f"Retrieving {name}, cid {cid}")
            c = Compound.from_cid(
                cid).to_dict(
                    properties=[
                        PubChem.ATOMS,
                        PubChem.BONDS
                    ]
                )
            # format for networkx and D3
            c[Graph.NODES] = c.pop(PubChem.ATOMS)
            c[Graph.LINKS] = c.pop(PubChem.BONDS)

            for node in c[Graph.NODES]:
                node[Graph.ID] = node.pop(PubChem.AID)
                node[Graph.SYMBOL] = node.pop(PubChem.ELEMENT)

            for link in c[Graph.LINKS]:
                link[Graph.SOURCE] = link.pop(PubChem.AID1)
                link[Graph.TARGET] = link.pop(PubChem.AID2)
                link[Graph.WEIGHT] = link.pop(PubChem.ORDER)

            here = path.dirname(path.realpath(__file__))
            with open(
                path.join(
                    here,
                    f'molecules/{name.replace(" ", "_")}.json'
                ), "w"
            ) as outfile:
                json.dump(
                    c,
                    outfile,
                    sort_keys=True,
                    indent=2
                )

    def _make_molecule(self, name) -> Molecule:
        here = path.dirname(path.realpath(__file__))
        
        # read in file
        with open(
            path.join(
                here,
                f'molecules/{name.replace(" ", "_")}.json'
            ), "r"
        ) as molfile:
            data=molfile.read()
        
        # parse file
        node_link=json.loads(data)

        # convert to networkx graph wrapped in
        # Molecule class
        M = Molecule(name=name, node_link=node_link)
        return M
