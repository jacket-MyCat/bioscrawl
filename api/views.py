from flask import Blueprint, jsonify, request

from . import A
from .atom import Atom
from .molecule import Molecule

main = Blueprint('main', __name__)

@main.route('/api/atoms')
def atoms():
    atom_obj = Atom()
    atoms = {}
    for a in atom_obj.get_atoms():
        atoms.update({a.symbol: a.__dict__})

    return jsonify(atoms)


@main.route('/api/answers')
def answers():
    answers = A.get_names()

    return jsonify(answers)


@main.route('/api/compare', methods=['POST'])
def compare():
    user_data = request.get_json()
    objective = user_data['objective']
    user_answer = Molecule(
        node_link=user_data['molecule']
    )
    correct_answer = A.get_answer(objective)

    return jsonify({
        'result': correct_answer.is_same(user_answer)
    })

@main.route('/api/give_up', methods=['POST'])
def give_up():
    user_data = request.get_json()
    objective = user_data['objective']
    correct_answer = A.get_answer(objective)
    node_link = correct_answer.to_node_link()

    return jsonify(node_link)
